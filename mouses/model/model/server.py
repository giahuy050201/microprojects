import io

import flask

app = flask.Flask(__name__)
from picamera import PiCamera


def gen(camera):
    my_stream = io.BytesIO()
    camera = PiCamera()
    while True:
        raw_frame = camera.capture(my_stream, 'jpeg')
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + raw_frame + b'\r\n')


@app.route('/video_feed')
def video_feed():
    return flask.Response(gen(Camera()),
                          mimetype='multipart/x-mixed-replace; boundary=frame')


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
