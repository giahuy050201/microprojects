import json

import PIL.Image as Image
import pkg_resources
import torch
from skimage import segmentation, filters, morphology, measure, io, color
from skimage.color import label2rgb
from torchvision.transforms import transforms

from model import models


def fps():
    model = models.get_model()

    img = Image.open('cam.jpg').convert('RGB')

    xform = transforms.Compose([
        transforms.Resize(256),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
    ])
    print(xform(img))
    frame = xform(img).unsqueeze(0)

    with pkg_resources.resource_stream('model', 'imagenet_1000.json') as f:
        labels = {int(k): v for k, v in json.load(f).items()}

    for _ in range(100):
        amax = (model(frame)[0].argmax())
        print(labels[amax.item()])

    torch.onnx.export(
        model,
        (frame,),
        'test.onnx',
    )


def find_img():
    import matplotlib.pyplot as plt
    import matplotlib.patches as mpatches

    image = io.imread('cam.jpg')
    image = color.rgb2gray(image)
    thresh = filters.threshold_otsu(image)

    bw = morphology.closing(image > thresh, morphology.square(3))
    cleared = segmentation.clear_border(bw)
    # label image regions
    label_image = measure.label(cleared)
    # to make the background transparent, pass the value of `bg_label`,
    # and leave `bg_color` as `None` and `kind` as `overlay`
    image_label_overlay = label2rgb(label_image, image=image, bg_label=0)

    fig, ax = plt.subplots(figsize=(10, 6))
    ax.imshow(image_label_overlay)

    for region in measure.regionprops(label_image):
        # take regions with large enough areas
        if region.area >= 100:
            # draw rectangle around segmented coins
            minr, minc, maxr, maxc = region.bbox
            rect = mpatches.Rectangle((minc, minr), maxc - minc, maxr - minr,
                                      fill=False, edgecolor='red', linewidth=2)
            ax.add_patch(rect)

    ax.set_axis_off()
    plt.tight_layout()
    plt.show()

if __name__ == '__main__':
    find_img()