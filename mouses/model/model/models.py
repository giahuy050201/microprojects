from torchvision.models import mnasnet0_5 as mdl


def get_model():
    res = mdl(True, True)
    res.eval()
    return res

