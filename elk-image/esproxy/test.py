import json
import sys
from datetime import datetime

import pytz
from paho.mqtt.subscribe import callback
import os
import requests
import traceback

es_node = None


def write_to_es(client, userdata, message):
    try:
        headers = {"Content-Type": "application/json"}
        payload = message.payload
        try:
            json_msg = json.loads(payload)
            if 'timestamp_ms' in json_msg:
                timestamp = datetime.fromtimestamp(json_msg['timestamp_ms'] / 1000, tz=pytz.UTC)
                json_msg['timestamp'] = timestamp.isoformat()

            payload = json.dumps(json_msg).encode('ascii')
        except json.JSONDecodeError:
            pass
        res = requests.post(
            es_node + "/" + message.topic + "/_doc",
            data=payload,
            headers=headers,
        )
        if res.status_code != 200:
            print(res)
            print(res.content)
    except Exception:
        traceback.print_exc()
        raise


if __name__ == "__main__":
    print("STARTING NOW")
    es_node = os.environ["ES_NODE"]
    node_name = os.environ["MQTT_ADDR"]
    tls = {
        "ca_certs": "/secrets/ca.crt",
    }
    with open("/secrets/creds.json") as f:
        sys.stdout.flush()
        f.seek(0, 0)
        creds = json.load(f)

    auth = {
        'username': creds['user'],
        'password': creds['password'],
    }

    callback(
        write_to_es,
        "#",
        hostname=node_name,
        client_id="proxy",
        tls=tls,
        auth=auth
    )
