#ifndef NETWORKING_HPP
#define NETWORKING_HPP

#include "secrets.hpp"

void setup_net();
bool send_message(const char *topic, const char *message);

#endif