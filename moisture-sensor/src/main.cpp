//
// Created by Richard Weiss on 2020-06-29.
//

#include <Arduino.h>
#define ARDUINOJSON_USE_LONG_LONG 1
#include <ArduinoJson.h>
#include <NTPClient.h>
#include <WiFiUdp.h>

#include "networking.hpp"

#ifdef NANO_33
const int MEASURE_PIN = A0;
const char* CLIENT = "nano";
#else
#ifdef NODEMCU
const int MEASURE_PIN = A0;
const char* CLIENT = "node";
#else
// A10 is GPIO pin 4, see
// https://github.com/espressif/arduino-esp32/blob/697d4ff7c4c35cf774aee8952126dd98daef3868/variants/esp32/pins_arduino.h
const int MEASURE_PIN = 36;
const char* CLIENT = "esp";
#endif
#endif

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "europe.pool.ntp.org", 3600, 60000);


void setup(void) {
    Serial.begin(9600);
    setup_net();
    pinMode(MEASURE_PIN, INPUT);
    timeClient.begin();
    timeClient.update();

    #ifdef ESPESP32DEV
    analogSetCycles(255);
    #endif
}

void loop(void) {
    static StaticJsonDocument<200> doc;

    if (random(0, 1000) <= 1) {
        Serial.println("Updating timestamp");
        timeClient.update();
    }

    int measure = analogRead(MEASURE_PIN);
    doc["measure"] = measure;
    long long epoch = timeClient.getEpochTime();
    doc["timestamp_ms"] = epoch * 1000;
    doc["client"] = CLIENT;

    String output;
    serializeJson(doc, output);

    Serial.println(output);
    send_message("test-moisture", output.c_str());
    delay(1000);
}