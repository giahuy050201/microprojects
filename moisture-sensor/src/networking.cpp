#include <Arduino.h>
#include <PubSubClient.h>
#ifdef NANO_33
#include <WiFiNINA.h>
#else
#ifdef NODEMCU
#include <ESP8266WiFi.h>
#else
#include <WiFiClientSecure.h>
#endif
#endif


#include "networking.hpp"

WiFiClientSecure wifi_client;
PubSubClient client(wifi_client);

void setup_net() {
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print("*");
  }

  Serial.println("Connected to network");

  #ifdef ESP32DEV
  wifi_client.setCACert((const char*)keys_ca_crt);
  #else
  wifi_client.setCACert(keys_ca_crt, keys_ca_crt_len);
  #endif
  client.setServer(broker, 1883);
}

bool send_message(const char* topic, const char *message) {
  static const char *clientid = "interface-client";
  int n_attempts = 0;
  while (n_attempts < 5) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(clientid, mqtt_user, mqtt_pass)) {
      Serial.println("connected");
      client.publish(topic, message);
      return true;
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      n_attempts++;
      delay(1000);
    }
  }
  return false;
}