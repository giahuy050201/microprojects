#ifndef SECRETS_HPP
#define SECRETS_HPP

extern const char *password;
extern const char *ssid;
extern const char *broker;

extern unsigned char keys_ca_crt[];
extern unsigned int keys_ca_crt_len;

extern const char *mqtt_user;
extern const char *mqtt_pass;

#endif