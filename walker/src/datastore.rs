use anyhow::{Context, Result};
use rusqlite::{params, Connection, NO_PARAMS};
use std::path::Path;
use std::io::{Read, Write};
use std::sync::Mutex;
use chrono::{DateTime, Utc};

use flate2::write::GzEncoder;
use flate2::Compression;
use flate2::read::GzDecoder;

/// The datastore is all the logic needed to save frames and events
/// into a database somewhere, and then retrieve them as needed when
/// we want to look at them.
///
/// We will use SQLite under the hood, however the intention is for
/// this to be fully hidden within the system, for safety...

#[derive(Debug)]
pub struct DataStoreBackend {
    connection: Connection,
    write_mutex: Mutex<()>
}


#[derive(Debug, Clone, Copy)]
pub enum DataStoreMessage {
    Frame([[f32; 8]; 8], DateTime<Utc>),
    Button(DateTime<Utc>),
}

impl DataStoreBackend {
    pub fn start<P: AsRef<Path>>(path: P) -> Result<DataStoreBackend> {
        let connection = Connection::open(path).context("Opening database connection")?;
        connection
            .execute("CREATE TABLE IF NOT EXISTS frame (frame BLOB, ts TIMESTAMP)", params![])
            .context("Creating frame table")?;
        connection
            .execute("CREATE TABLE IF NOT EXISTS event (event TEXT, ts TIMESTAMP)", params![])
            .context("Creating events table")?;
        let write_mutex = Mutex::new(());
        Ok(DataStoreBackend { connection, write_mutex})
    }

    fn record_frame(&self, frame: [[f32; 8]; 8], timestamp: &DateTime<Utc>) -> Result<()> {
        let blob_text: Vec<u8> = rmp_serde::encode::to_vec(&frame).context("Encoding frame")?;
        let mut output_vec = Vec::new();
        {
            let mut encoder = GzEncoder::new(&mut output_vec, Compression::best());
            encoder.write_all(&blob_text)?;
            encoder.finish()?;
        }
        // Lock the mutex and hold the lock to the end of the
        // write. This is to prevent SQLITE database locked errors.
        let _ = self.write_mutex.lock();
        self.connection
            .execute(
                "INSERT INTO frame VALUES (?1, ?2)",
                params![output_vec, timestamp]
            )
            .context("Preparing statement")?;
        Ok(())
    }


    fn record_event(&self, event: &str, timestamp: &DateTime<Utc>) -> Result<()> {
        // Lock the mutex and hold the lock to the end of the
        // write. This is to prevent SQLITE database locked errors.
        let _ = self.write_mutex.lock();

        self.connection
            .execute(
                "INSERT INTO event VALUES (?1, ?2)",
                params![event, timestamp]
            )
            .context("Preparing statement")?;
        Ok(())
    }

    pub fn record(&self, event: DataStoreMessage) -> Result<()> {
        match event {
            DataStoreMessage::Frame(f, ts) => self.record_frame(f, &ts),
            DataStoreMessage::Button(ts) => self.record_event("button", &ts)
        }
    }

    pub fn all_events(&self) -> Vec<DataStoreMessage> {
        let mut row_stmt = self.connection
            .prepare("\
            SELECT 'frame', frame as data, ts FROM frame \
            UNION \
            SELECT 'event', event as data, ts FROM event\
            ").unwrap();

        let events = row_stmt.query_map(NO_PARAMS, |row| {
            let row_type: String = row.get(0).unwrap();
            let row_ts: DateTime<Utc> = row.get(2).unwrap();
            let new_event = match () {
                () if row_type == "frame" => {
                    let compressed_row_content: Vec<u8> = row.get(1).unwrap();
                    let mut decompressor = GzDecoder::new(&compressed_row_content[..]);
                    let mut row_content = Vec::new();
                    decompressor.read_to_end(&mut row_content).unwrap();

                    DataStoreMessage::Frame(
                        rmp_serde::decode::from_read_ref(&row_content).unwrap(),
                        row_ts
                    )
                },
                () if row_type == "event" => DataStoreMessage::Button(row_ts),
                _ => unreachable!()
            };

            Ok(new_event)
        }).unwrap();

        let mut res = Vec::new();
        for r in events {
            res.push(r.unwrap())
        }
        return res
    }
}