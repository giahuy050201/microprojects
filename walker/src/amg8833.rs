#![allow(dead_code)]

use anyhow::{Context, Result};

#[cfg(feature = "rppal")]
use rppal::i2c::I2c;

// Dummy implementation of AMG, just for testing...
#[cfg(not(feature = "rppal"))]
struct I2c {}

#[cfg(not(feature = "rppal"))]
impl I2c {
    pub fn new() -> Result<I2c> {
        return Ok(I2c{})
    }

    pub fn set_slave_address(&mut self, _slave_address: u16) -> Result<()> {
        Ok(())
    }

    pub fn write(&mut self, buffer: &[u8]) -> Result<usize> {
        return Ok(buffer.len())
    }

    pub fn read(&mut self, buffer: &mut [u8]) -> Result<usize> {
        for i in 0..buffer.len() {
            buffer[i] = 8;
        }

        Ok(buffer.len())
    }
}

static AMG8833_PCTL: u8 = 0x00;
static AMG8833_RST: u8 = 0x01;
static AMG8833_FPSC: u8 = 0x02;
static AMG8833_INTC: u8 = 0x03;
static AMG8833_STAT: u8 = 0x04;
static AMG8833_SCLR: u8 = 0x05;
static AMG8833_AVE: u8 = 0x07;
static AMG8833_INTHL: u8 = 0x08;
static AMG8833_INTHH: u8 = 0x09;
static AMG8833_INTLL: u8 = 0x0A;
static AMG8833_INTLH: u8 = 0x0B;
static AMG8833_IHYSL: u8 = 0x0C;
static AMG8833_IHYSH: u8 = 0x0D;
static AMG8833_TTHL: u8 = 0x0E;
static AMG8833_TTHH: u8 = 0x0F;

//  Interrupt result registers
static AMG8833_INT0: u8 = 0x10; // threshold interrupt for pixels 0 - 7, etc;
static AMG8833_INT1: u8 = 0x11;
static AMG8833_INT2: u8 = 0x12;
static AMG8833_INT3: u8 = 0x13;
static AMG8833_INT4: u8 = 0x14;
static AMG8833_INT5: u8 = 0x15;
static AMG8833_INT6: u8 = 0x16;
static AMG8833_INT7: u8 = 0x17;

// 64, 16-bit words for the 8 x 8 IR array
static AMG8833_DATA01L: u8 = 0x80; // pixel 1 low byte;
static AMG8833_DATA01H: u8 = 0x81; // pixel 1 high byte;
                                   //  ...
                                   // These go up and up until we reach below:
static AMG8833_DATA64L: u8 = 0xFE; // pixel 64 low byte;
static AMG8833_DATA64H: u8 = 0xFF; // pixel 64 high byte;

static AMG8833_ADDRESS: u8 = 0x69; // 0x68 when ADO = LOW, 0x69 when ADO = HIGH;

//Use these with the Power Control Register: AMG8833_PCTL (0x00)

static AMG8833_NORMAL_MODE: u8 = 0b00000000; //;
static AMG8833_SLEEP_MODE: u8 = 0b00010000;
static AMG8833_SB_60I: u8 = 0b00100000;
static AMG8833_SB_10I: u8 = 0b00100001;

// Reset Register AMG8833_RST      0x01
static AMG8833_FLAG_RESET: u8 = 0b00110000;
static AMG8833_SOFTWARE_RESET: u8 = 0b00111111;

//Frame rates for the MG8833_FPSC     0x02 register
static AMG8833_1FPS: u8 = 0b00000001;
static AMG8833_10FPS: u8 = 0b00000000; //default!;

trait Helpers {
    fn read_byte(&mut self, reg_address: u8) -> Result<u8>;
    fn read_bytes(&mut self, reg_address: u8, count: usize) -> Result<Vec<u8>>;
}

impl Helpers for I2c {
    fn read_byte(&mut self, reg_address: u8) -> Result<u8> {
        self.write(&[reg_address]).context("Requesting byte")?;
        let mut out_vec: [u8; 1] = [0];
        self.read(&mut out_vec).context("Reading byte")?;
        Ok(out_vec[0])
    }

    fn read_bytes(&mut self, reg_address: u8, count: usize) -> Result<Vec<u8>> {
        self.write(&[reg_address]).context("Requesting bytes")?;
        let mut out_vec = Vec::with_capacity(count);
        for _ in 0..count {
            out_vec.push(0);
        }
        self.read(&mut out_vec).context("Reading bytes")?;
        Ok(out_vec)
    }
}

pub struct AMG8833 {
    i2c: I2c,
    address: u16,
}

fn twovec_to_i16(a: u8, b: u8) -> i16 {
    ((b as i16) << 8) | a as i16
}

impl AMG8833 {
    pub fn new() -> Result<AMG8833> {
        let mut i2c = I2c::new().context("Opening i2c")?;
        i2c.set_slave_address(AMG8833_ADDRESS as u16)
            .context("Setting address")?;
        let address = AMG8833_ADDRESS as u16;
        let mut res = AMG8833 { i2c, address };
        res.i2c
            .write(&[AMG8833_RST, AMG8833_SOFTWARE_RESET])
            .context("Resetting AMG8833")?;
        res.i2c
            .write(&[AMG8833_PCTL, AMG8833_NORMAL_MODE])
            .context("Setting mode to normal")?;
        res.i2c
            .write(&[AMG8833_FPSC, AMG8833_10FPS])
            .context("Setting FSP")?;
        Ok(res)
    }

    pub fn read_thermistor(&mut self) -> Result<f64> {
        let res = self
            .i2c
            .read_bytes(AMG8833_TTHL, 2)
            .context("Reading thermistor")?;
        let temp: i16 = twovec_to_i16(res[0], res[1]);
        let real_temp = (temp as f64) * 0.0625;

        Ok(real_temp)
    }

    pub fn read_pixels(&mut self) -> Result<[[f32; 8]; 8]> {
        let raw_data = self
            .i2c
            .read_bytes(AMG8833_DATA01L, 64 * 2)
            .context("Reading raw data")?;
        let mut res = [[0.0; 8]; 8];
        for r in 0..8 {
            for c in 0..8 {
                let start_ix = (r * 8 + c) * 2;
                let unscaled_value = twovec_to_i16(raw_data[start_ix], raw_data[start_ix + 1]);
                let scaled_value = (unscaled_value as f32) * 0.25;
                res[r][c] = scaled_value;
            }
        }

        Ok(res)
    }
}
